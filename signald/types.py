import attr


@attr.s
class Attachment:
    stored_filename = attr.ib(type=str)
    content_type = attr.ib(type=str, default=None)
    id = attr.ib(type=str, default=None)
    size = attr.ib(type=int, default=None)


@attr.s
class Message:
    username = attr.ib(type=str)
    source = attr.ib(type=str)
    text = attr.ib(type=str)
    source_device = attr.ib(type=int, default=0)
    timestamp = attr.ib(type=int, default=None)
    timestamp_iso = attr.ib(type=str, default=None)
    expiration_secs = attr.ib(type=int, default=0)
    is_receipt = attr.ib(type=bool, default=False)
    attachments = attr.ib(type=list, default=[])
    quote = attr.ib(type=str, default=None)

@attr.s
class Contact:
    name = attr.ib(type=str)
    number = attr.ib(type=str)
    