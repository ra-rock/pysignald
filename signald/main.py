import traceback

import magic
import time
import json
import random
import re
import socket
import qrcode
import json
import threading
from datetime import datetime, timedelta
from typing import Iterator, List  # noqa

from .types import Attachment, Message

# We'll need to know the compiled RE object later.
RE_TYPE = type(re.compile(""))


def readlines(s: socket.socket) -> Iterator[bytes]:
    "Read a socket, line by line."
    buf = []  # type: List[bytes]
    while True:
        char = s.recv(1)
        if not char:
            raise ConnectionResetError("connection was reset")

        if char == b"\n":
            yield b"".join(buf)
            buf = []
        else:
            buf.append(char)


class Signal:
    def __init__(self, username, socket_path="/var/run/signald/signald.sock"):
        self.username = username
        self.socket_path = socket_path
        self._chat_handlers = []
        self.outbox = []
        #l_contacts = json.loads(CONTACTS.encode("utf-8", "ignore").decode("utf-8"))
        #l_contacts_new = json.loads(NEW_CONTACTS.encode("utf-8", "ignore").decode("utf-8"))
        
        
        self._beta_testers = [
            {"name":"M", "number":"+15166702018"},
            {"name":"Z", "number":"+14159102390"},
            {"name":"S", "number":"+16468973446"},
            {"name":"P", "number":"+19178381967"},
            {"name":"Y", "number":"+18083897454"},
            {"name":"N", "number":"+19175950735"},
            {"name":"I", "number":"+17876776273"},
        ]
        
        self.data = [];
        with open("/home/qr/contacts") as json_file:
            self.data = json.load(json_file)

    def _get_id(self):
        "Generate a random ID."
        return "".join(random.choice("abcdefghijklmnopqrstuvwxyz0123456789") for _ in range(10))

    def _get_socket(self) -> socket.socket:
        "Create a socket, connect to the server and return it."
        
        # Support TCP sockets on the sly.
        if isinstance(self.socket_path, tuple):
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        else:
            s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        s.connect(self.socket_path)
        return s

    def _send_command(self, payload: dict, block: bool = True) -> bool:
        s = self._get_socket()
        msg_id = self._get_id()
        payload["id"] = msg_id
        s.recv(1024)  # Flush the buffer.
        s.send(json.dumps(payload).encode("UTF-8") + b"\n")

        if not block:
            s.close()
            return

        response = s.recv(4 * 1024)
        for line in response.split(b"\n"):
            print("\nRESPONSE\n")
            print(line)
            if msg_id.encode("UTF-8") not in line:
                continue

            data = json.loads(line.decode("UTF-8"))

            if data.get("id") != msg_id:
                continue

            if data["type"] == "unexpected_error":
                s.close()
                return False
                #raise ValueError("unexpected error occurred")
        
        s.close()
        return True

    def link(self):
        payload = {"type": "link"}
        s = self._get_socket()
        msg_id = self._get_id()
        payload["id"] = msg_id
        s.recv(1024)  # Flush the buffer.
        s.send(json.dumps(payload).encode("UTF-8") + b"\n")

        response = s.recv(4 * 1024)
        for line in response.split(b"\n"):
            if "linking_uri".encode("UTF-8") not in line:
                continue
            data = json.loads(line.decode("UTF-8"))
            uri = data.get("data").get("uri")
            img = qrcode.make(uri)
            img.save("/home/qr/qrcode.jpeg")
            
        s.close()
            

    def sync_contacts(self):
        payload = {"type": "sync_contacts", "username": self.username}
        s = self._get_socket()
        msg_id = self._get_id()
        payload["id"] = msg_id
        s.recv(1024)  # Flush the buffer.
        s.send(json.dumps(payload).encode("UTF-8") + b"\n")
        #s.recv(1024)  # Flush the buffer.
        #s.send(json.dumps({"type": "subscribe", "username": self.username}).encode("UTF-8") + b"\n")
        
        response = s.recv(4 * 1024)
        for line in response.split(b"\n"):
            if "linking_uri".encode("UTF-8") not in line:
                continue
            data = json.loads(line.decode("UTF-8"))
            print(data)
            
        s.close()
      
    def compare_contacts(self):
        #self.sync_contacts()
        
        payload = {"type": "list_contacts", "username": self.username}
        s = self._get_socket()
        msg_id = self._get_id()
        payload["id"] = msg_id
        s.recv(1024)  # Flush the buffer.
        s.send(json.dumps(payload).encode("UTF-8") + b"\n")

#        response = s.recv(8 * 1024)
#        for line in response.split(b"\n"):
#            print(line)
#        print(response)
        print("hi")
        response = b""
        for i in range(64):
            print(i)
            response += s.recv(1024)
            try:
                data = json.loads(response.decode("UTF-8"))
                #print(data)
                
                l_contacts =  self.data
                l_contacts_new = data.get("data")
                
                old = set([d['number'] for d in l_contacts])
                new = set([d['number'] for d in l_contacts_new])
                
                print("length: ")
                print(len(old))
                print("\n length: ")
                print(len(new))
                
                diff = new - old
                diff_symmetric = new.symmetric_difference(old)
                
                d = [o for o in l_contacts_new if o["number"] in diff_symmetric]
                print(d)
                
                break
            except:
                continue
        
        #s.send(json.dumps({"type": "unsubscribe", "username": self.username}).encode("UTF-8") + b"\n")
        s.close()

    def register(self, voice=False):
        """
        Register the given number.

        voice: Whether to receive a voice call or an SMS for verification.
        """
        payload = {"type": "register", "username": self.username, "voice": voice}
        self._send_command(payload)

    def verify(self, code: str):
        """
        Verify the given number by entering the code you received.

        code: The code Signal sent you.
        """
        payload = {"type": "verify", "username": self.username, "code": code}
        self._send_command(payload)
        
    def get_contacts(self, testing = True) -> List[dict]:
        if testing:
            return self._beta_testers
            #return [{"name":"I", "number":"+17876776273"}]
        
        #if len(self._contacts):
        #    return self._contacts
        
        if len(self.data) > 0:
            return self.data
        
        return []

        payload = {"type": "list_contacts", "username": self.username}
        s = self._get_socket()
        #s.send(json.dumps({"type": "subscribe", "username": self.username}).encode("UTF-8") + b"\n")
        msg_id = self._get_id()
        payload["id"] = msg_id
        s.recv(1024)  # Flush the buffer.
        s.send(json.dumps(payload).encode("UTF-8") + b"\n")
        
        response = b""
        for i in range(64):
            response += s.recv(1024)
            try:
                data = json.loads(response.decode("UTF-8"))
                self._contacts = data.get("data")
                break
            except:
                continue
        
        #s.send(json.dumps({"type": "unsubscribe", "username": self.username}).encode("UTF-8") + b"\n")
        s.close()
        
        return self._contacts
        
    def delay_broadcast(self, hours):
        now = datetime.now()
        run_at = now + timedelta(hours=hours)
        delay = (run_at - now).total_seconds()
        threading.Timer(delay, self.broadcast_menu).start()

    def broadcast_menu(self):
        
        menu_filename = "/home/qr/projects/qr-bot/menu"
        event_filename = "/home/qr/projects/qr-bot/event"
        t = b"Greetings fam \\u2728 We're back from a short midsommar hiatus.  Blessing your day with an interim/abridged version of September's menu:\\n\\nFlower\\nFruit Punch\\nGorilla Glue #4\\nGelato\\nOrange Durban Poison\\nPink Panties\\nAlien OG\\nForbidden Fruit\\n\\n** As always same day delivery available, appointments preferred**"
        t2 = b"Also, we'd love to see you! Next friday we are holding our Fall speakeasy in a new locale in the West Village. \\nUse code RoseCasa for $15 off \\nentry:https://rosebudxmicasa.splashthat.com/\\n\\nHope to see you there\\U0001f339"
        t = t.decode("unicode-escape")
        t2 = t2.decode("unicode-escape")
        message = Message(
            username = self.username,
            source = "",
            #text = "Check out the new menu for the month! Effective May 8th.",
            text = t,
            attachments = [
                Attachment(stored_filename='/home/qr/Downloads/1503497014530879064.jpg',
                           content_type='image/jpeg'),]
        )
        
        message2 = Message(
            username = self.username,
            source = "",
            #text = "Check out the new menu for the month! Effective May 8th.",
            text = t2,
            attachments = [
                Attachment(stored_filename='/home/qr/Downloads/1956720011211224930.jpg',
                           content_type='image/jpeg'),]
        )
        
        limit = 0
        begin = 0
        blocked = ["+18186659967","+16095021321"]
        #contacts = [{"name":"I", "number":"+17876776273"}]
        contacts = self.get_contacts()[begin:] #Dev
        #contacts = self.get_contacts(False)[begin:] #Production
        end = len(contacts)
        broadcast_range = range(begin, end)
        #broadcast_range = [10, 11, 13, 24, 35, 122, 130, 154, 155, 163, 165, 167, 168, 169, 171, 174, 180, 181, 185, 192, 193, 195, 199, 205, 212, 222, 223, 231, 238, 247, 255, 257, 258, 263, 268, 270, 271, 274, 275, 284, 289, 293, 297, 299, 305, 308, 315, 317, 319, 323, 329, 330, 336, 337, 342, 343, 345, 346, 354, 363, 373, 375, 382, 388, 391, 396, 397, 405, 408, 419, 437, 439, 450, 453, 456, 466, 485, 487, 491, 499, 502, 503, 508, 510, 515, 524, 525, 559, 575, 601, 631, 639, 645, 662, 667, 689, 711, 723, 775, 776, 841, 850, 895, 955, 969, 981, 996, 1010, 1048, 1062, 1073, 1074]
       
        future = [];
        for i in broadcast_range:
            contact = contacts[i]
            print("contacts:" + str(contact))
            if contact["number"] in blocked:
                print("\n\n BLACKLIST: " + contact["number"])
                time.sleep(5)
                continue
            message.source = contact["number"]
            message2.source = contact["number"]
            
            future.append(message.copy())
            future.append(message2.copy())
            
            print("\n\n" + str(i))
            print(contact["name"], contact["number"])
                
        print(future)
        while limit < 3:
            future = self.broadcast(messages=future)
            limit += 1
        
        for f in future:
            print(f.source)
            
    def broadcast(self, messages):
        future = []
        for message in messages:
            #successful = self.send_message(message=message)
            successful = False
            if not successful:
                future.append(message)
                delay = random.uniform(5,10)
            else:
                delay = random.uniform(2,5)
            time.sleep(delay)
            
        return future
            
    def trust(self):
        payload = {"type": "get_identities", "username": self.username}
        s = self._get_socket()
        
        contacts = self.get_contacts(False)
        broadcast_range = [10, 11, 13, 24, 35, 122, 130, 154, 155, 163, 165, 167, 168, 169, 171, 174, 180, 181, 185, 192, 193, 195, 199, 205, 212, 222, 223, 231, 238, 247, 255, 257, 258, 263, 268, 270, 271, 274, 275, 284, 289, 293, 297, 299, 305, 308, 315, 317, 319, 323, 329, 330, 336, 337, 342, 343, 345, 346, 354, 363, 373, 375, 382, 388, 391, 396, 397, 405, 408, 419, 437, 439, 450, 453, 456, 466, 485, 487, 491, 499, 502, 503, 508, 510, 515, 524, 525, 559, 575, 601, 631, 639, 645, 662, 667, 689, 711, 723, 775, 776, 841, 850, 895, 955, 969, 981, 996, 1010, 1048, 1062, 1073, 1074]
        
        s.recv(1024)  # Flush the buffer.
        for i in broadcast_range:
            payload["recipientNumber"] = contacts[i]["number"]
            s.send(json.dumps(payload).encode("UTF-8") + b"\n")
            response = s.recv(4* 1024)
            try:
                data = json.loads(response.decode("UTF-8"))
                for identity in data["data"]["identities"]:
                    if identity["trust_level"] == "UNTRUSTED":
                        print(contacts[i]["number"])
                        trust_payload = {"type": "trust",
                                         "username": self.username,
                                         "recipientNumber": identity["username"],
                                         "fingerprint": identity["safety_number"]}
                        s.send(json.dumps(trust_payload).encode("UTF-8") + b"\n")
                        print(s.recv(4* 1024))
                continue
            except:
                continue
                

        s.close()
        
        
    def receive_messages(self) -> Iterator[Message]:
        "Keep returning received messages."
        s = self._get_socket()
        s.send(json.dumps({"type": "subscribe", "username": self.username}).encode("UTF-8") + b"\n")
        
        m = magic.Magic(mime_encoding=True)
        for line in readlines(s):
            print(line)
            try:
                decoded = line.decode()
                message = json.loads(decoded)
            except json.JSONDecodeError:
                print("Invalid JSON")

            if message.get("type") != "message" or (
                not message["data"]["isReceipt"] and message["data"].get("dataMessage") is None
            ):
                # If the message type isn't "message", or if it's a weird message whose
                # purpose I don't know, return. I think the weird message is a typing
                # notification.
                continue

            message = message["data"]
            data_message = message["dataMessage"] if message.get("dataMessage") else {}
            
            if message.get("dataMessage"):
                print(data_message.get("message").encode("unicode_escape"))
            
            yield Message(
                username=message["username"],
                source=message["source"],
                text=data_message.get("message"),
                source_device=message["sourceDevice"],
                timestamp=data_message.get("timestamp"),
                timestamp_iso=message["timestampISO"],
                expiration_secs=data_message.get("expiresInSeconds"),
                is_receipt=message["isReceipt"],
                attachments=[
                    Attachment(
                        content_type=attachment["contentType"],
                        id=attachment["id"],
                        size=attachment["size"],
                        stored_filename=attachment["storedFilename"],
                    )
                    for attachment in data_message.get("attachments", [])
                ],
            )

    def send_message(self, message: Message, block: bool = True) -> bool:
        payload = {
            "type": "send",
            "username": message.username,
            "recipientNumber": message.source,
            "messageBody": message.text,
            "attachments": [
                {
                    "contentType": attachment.content_type,
                    "id": attachment.id,
                    "size": attachment.size,
                    "filename": attachment.stored_filename,
                }
                for attachment in message.attachments
            ],
        }
        print("\nSENDING:\n")
        print(payload)
        r = self._send_command(payload, block)
        print("success:")
        print(r)
        return r

    def send_text(self, recipient: str, text: str, block: bool = True) -> None:
        """
        Send a message.

        recipient: The recipient's phone number, in E.123 format.
        text:      The text of the message to send.
        block:     Whether to block while sending. If you choose not to block, you won't get an exception if there
                   are any errors.
        """
        payload = {"type": "send", "username": self.username, "recipientNumber": recipient, "messageBody": text}
        self._send_command(payload, block)

    def send_attachment(self, recipient: str, filename: str, block: bool = True) -> None:
        """
        Send a message.

        recipient: The recipient's phone number, in E.123 format.
        text:      The text of the message to send.
        block:     Whether to block while sending. If you choose not to block, you won't get an exception if there
                   are any errors.
        """
        payload = {"type": "send", "username": self.username, "recipientNumber": recipient, "attachments": [
            {"filename": filename}
            ]}
        self._send_command(payload, block)
        
    def chat_handler(self, regex, order=100):
        """
        A decorator that registers a chat handler function with a regex.
        """
        if not isinstance(regex, RE_TYPE):
            regex = re.compile(regex, re.I)

        def decorator(func):
            self._chat_handlers.append((order, regex, func))
            # Use only the first value to sort so that declaration order doesn't change.
            self._chat_handlers.sort(key=lambda x: x[0])
            return func

        return decorator

    def run_chat(self):
        """
        Start the chat event loop.
        """
        for message in self.receive_messages():
            if message.is_receipt:
                continue
            
            if not message.text:
                message.text = ""

            for _, regex, func in self._chat_handlers:
                match = re.search(regex, message.text)
                if not match:
                    continue

                try:
                    reply = func(message, match)
                except:  # noqa - We don't care why this failed.
                    print(traceback.format_exc())
                    for message in self.outbox:
                        print(message.future)
                    continue

                if isinstance(reply, tuple):
                    stop, reply = reply
                else:
                    stop = True

                if isinstance(reply, str):
                    self.send_text(recipient=message.source, text=reply)
                elif isinstance(reply, Message):
                    self.send_message(message=reply)
                #else:
                #    print(reply)

                if stop:
                    # We don't want to continue matching things.
                    break
